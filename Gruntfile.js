module.exports = function(grunt) {
grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    clean: {
        dist: {
            src: ['dist'],
        },
    },

    exec: {
        deploy: {
            cmd: function(remotePath) {
                if (remotePath) {
                    return 'rsync -avzh --delete --progress ./dist/ ' + remotePath;
                }
                return;
            }
        },
    },

    copy: {
        dist: {
            expand: true,
            cwd: '.',
            src: ['**', '!.**', '!node_modules/**', '!Gruntfile.js', '!package.json'],
            dest: 'dist/',
        },
    },
});

    // Plugins
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-exec');

    // Tasks
    grunt.registerTask('default', ['dist']);
    grunt.registerTask('dist', [
        'clean',
        'copy'
    ]);
};
