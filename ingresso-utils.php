<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/**
 * Plugin Name: Ingresso Utils
 * Plugin URI: https://bitbucket.org/ricardomoro/ingresso-utils/
 * Description: Utilidades para o Portal de Ingresso do IFRS.
 * Version: 1.4
 * Author: Ricardo Moro
 * Author URI: http://ifrs.edu.br/
 * License: GPLv3
 */

// Taxonomy Single Term
require_once('taxonomy-single-term/class.taxonomy-single-term.php');

// Taxonomies
require_once('taxonomies/campus-taxonomy.php');
require_once('taxonomies/modalidade-taxonomy.php');
require_once('taxonomies/turno-taxonomy.php');
require_once('taxonomies/formaingresso-taxonomy.php');

// Edital Post Type
require_once('post-types/edital-posttype.php');

// Curso Post Type
require_once('post-types/curso-posttype.php');

// Resultado Post Type
require_once('post-types/resultado-posttype.php');

// Multisite SSL Verification Workaround
add_filter('https_ssl_verify', '__return_false');
add_filter('https_local_ssl_verify', '__return_false');
